+++
title = "Kiel bone kunsidi?"
path = "10-kiel-bone-kunsidi"
aliases = ["10"]
weight = 10
date = 2017-11-22T22:53:24Z

[extra]
duration = "38:07"
mp3 = { filename = "LaBoRen-P10-kiel-bone-kunsidi-20171122-223242.mp3", size = 39191819 }
mp3_lofi = { filename = "LaBoRen-P10-kiel-bone-kunsidi-malpeza-20171122-223851.mp3", size = 18296998 }
ogg = { filename = "LaBoRen-P10-kiel-bone-kunsidi-20171122-223601.ogg", size = 28910629 }

[[extra.comments]]
pk = 17
name = "Edison José Nunes de Lima"
color = "hsl(255, 100%, 41%)"
submit_date = "2017-12-03T01:22:52"
comment = """Mi pensas ke iun povus instrui Esperanton  al geknaboj aux gejunuloj. Ili havas bonan memoroj  Kaj ili ŝatas paroli lingvon kion iliaj amikoj kaj gepatroj ne konas. Eble la gepatroj mem interesos.  "instruu la infanoj la vojo kien ili devas trairi; kaj kiam ili kreskos ne devios el ĝi. Antaux dudek jaroj mi lernigis geknabojn iu tempo. Post mi estis laboranta  em alia sxtato. Do, finis la instruado. Sed mi jam enkonti juj  el ili kaj mi estis salutito en Esperanto. Nun se mi povos de nove paroli Esperanton fluece (80 jaraĝe) mi lerniĝos geknaboj kaj gejunuloj."""
+++

La dua serio revenas per epizodo pri kiel bone, do fakte efike kunsidi. 

Ĉu vi sciis, ke por bona kunsido vi ne bezonas ĉiujn el via teamo?
Kaj tion, ke la antaŭpreparo same gravas kiel la kunsido mem?

Decidu kiel vi komunikos en via teamo, kaj poste havu klaran planon por kiam, pri kio kaj kun kiu kunsidi. Vi ĉesos havi senfinajn-longegajn-tedegajn kunsidojn post la aŭskultado de tiu ĉi epizodo, ĉar vi facile povos kompreni kion ŝanĝi por havi la rezultojn, kiujn vi bezonas.

Ni ĝojas reveni en vian vivon, kaj daŭrigi la podkastojn!

