+++
title = "Enkonduko de la nova serio pri denaskuleco"
path = "200-nova-serio-enkonduko"
aliases = ["200"]
weight = 200
date = 2020-11-27T13:00:00Z

[extra]
duration = "52:39"
mp3 = { filename = "serio_enkonduko_alta.mp3", size = 10872384 }
mp3_lofi = { filename = "serio_enkonduko.mp3", size = 2174640 }
ogg = { filename = "serio_enkonduko.ogg", size = 3190824 }
+++


Rebonvenon al LaBoRen!

En la enkonduka epizodo vi aŭdos pri kio temas la nova serio de La Bona Renkontiĝo.
Ni okupiĝas pri la temo de denaskuleco.
Aperos intervjuoj kun diversaj homoj: aktivuloj, denaskuloj, gepatroj rilate al la temo de edukado kun Esperanto.

La projekton helpas ESF (esperantic.org) kaj ni tre dankas la kunlaboradon al ili.
