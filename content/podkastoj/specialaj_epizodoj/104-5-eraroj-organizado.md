+++
title = "La 5 plej grandaj eraroj de organizado"
path = "104-5-eraroj-organizado"
aliases = ["104"]
weight = 104
date = 2018-04-26T10:33:09Z

[extra]
duration = "18:40"
mp3 = { filename = "LaBoRen-P104-5-eraroj-organizado-20180426-095801.mp3", size = 37503377 }
mp3_lofi = { filename = "LaBoRen-P104-5-eraroj-organizado-malpeza-20180426-095801.mp3", size = 8961670 }
ogg = { filename = "LaBoRen-P104-5-eraroj-organizado-20180426-095801.ogg", size = 14527718 }
+++

En tiu ĉi speciala epizodo mi pridiskutas kiuj estas la kvin plej grandaj eraroj dum la organizado de junularaj eventoj kaj laŭ eble kiel plej bone eviti ilin. 

Ĉu vi konsentas pri la elekto? Ĉu vi pensas, ke grava eraro mankas? Ĉu vi ŝanĝus la sinsekvon en la listo?
Via opinio gravas! Skribu al ni en twitter aŭ facebook, uzante #LaBoRen.

