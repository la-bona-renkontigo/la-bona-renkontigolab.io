+++
title = "Subtenu LaBoRen!"
+++

<div class="column is-two-thirds mx-auto">
{{ quote(text="Invitu la soifan organizanton al la trinkejo por malvarma senalkohola biero!", author="ordonis la Moŝta Trinkejestro")}}

{{ support(platform="liberapay") }}

{{ support(platform="paypal") }}
</div>
