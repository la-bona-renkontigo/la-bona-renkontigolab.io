+++
title = "Varbu por via renkontiĝo!"
path = "14-varbado"
aliases = ["14"]
weight = 14
date = 2018-01-18T11:11:02Z

[extra]
duration = "19:02"
mp3 = { filename = "LaBoRen-P14-varbado-20180118-110337.mp3", size = 24519221 }
mp3_lofi = { filename = "LaBoRen-P14-varbado-malpeza-20180118-110337.mp3", size = 9137631 }
ogg = { filename = "LaBoRen-P14-varbado-20180118-110337.ogg", size = 14398051 }
+++

Kiel varbi por via renkontiĝo?

Ne estas sekreto, ke Esperantio ankoraŭ ne estas grandega, do persona varbado estas la plej bona metodo por inviti homojn al via renkontiĝo.
Mi rakontas iomete pri tio, kiel oni varbis antaŭ la interreto ekzistis, kaj nun kun la apero de sociaj retejoj uzu la eblecojn, kiuj sinprezentas. La sekreto estas, ke vi daŭre devas varbi, ne nur unu aŭ du foje.
Kio okazas, se vi ne povas mem fari tion? Do petu aliajn homojn por helpi! Amikojn, kaj instruistojn de la lingvo por varbi inter komencantoj-novuloj.

