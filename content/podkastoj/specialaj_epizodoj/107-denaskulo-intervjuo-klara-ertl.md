+++
title = "La bonŝanca denaskulo - intervjuo kun Klára Ertl"
path = "107-denaskulo-intervjuo-klara-ertl"
aliases = ["107"]
weight = 107
date = 2018-06-06T09:52:05Z

[extra]
duration = "51:36"
mp3 = { filename = "LaBoRen-P107-denaskulo-intervjuo-klara-ertl-20180606-093153.mp3", size = 98377938 }
mp3_lofi = { filename = "LaBoRen-P107-denaskulo-intervjuo-klara-ertl-malpeza-20180606-094834.mp3", size = 24769514 }
ogg = { filename = "LaBoRen-P107-denaskulo-intervjuo-klara-ertl-20180606-095044.ogg", size = 35578569 }

[[extra.comments]]
pk = 112
name = "Noviano"
color = "hsl(257, 100%, 41%)"
submit_date = "2018-06-07T08:10:40.086271"
comment = """Ĉi tiu estas mia unufoje aŭskultas al la podkaston. Mi ne estas flua en la lingvo, tamen mi povas kompreni kio vi diras pli facila. Ambaŭ de ilia akcento estas tre klara kaj vi parolas la lingvo tre bele. <br>
<br>
Dankon kaj bonege Intervuo"""
+++

Stela intervjuas Klára Ertl pri denaskuleco. Ŝi estas juna psikologo de Amsterdamo, kaj havas multe da similaj spertoj kiel Stela. Estis tre gaja babilado pri familio, lingvoj, landoj kaj kulturoj.

En tiu ĉi epizodo ni babiladis:

- pri ŝiaj gepatroj, la amuzaj historioj pri kiel ili eklernis Esperanton
- lingvoj, lingvodinamikoj kun la fratoj, kaj kiel tio ŝanĝiĝis pro translokiĝo de unu lando al la alia.
- Ni parolis pri: senhejmuloj kaj rifuĝintoj, pri ŝiaj studoj kaj volontulado,
- pri la simileco inter psikologio – kion ŝi studis, kaj sociologio – kion mi studis.
- Ni profunde pridiskutis kiel esti denaskulo influas la vivon, kaj kion oni havas, kion oni maltrafas, sed ĝenerale kial oni bonŝancas esti denaskulo.

Kaj kompreneble ankoraŭ multe pli.

Por trovi aldonajn notojn kaj aliajn menciitajn aferojn pri nia babilado iru al https://stelachiamnurkritikas.wordpress.com/2018/06/06/la-bonsanca-denaskulo-intervjuo-kun-klara-ertl/

