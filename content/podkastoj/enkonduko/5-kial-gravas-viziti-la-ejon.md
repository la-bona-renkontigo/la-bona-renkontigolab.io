+++
title = "Kial gravas viziti la ejon?"
path = "5-kial-gravas-viziti-la-ejon"
aliases = ["5"]
weight = 5
date = 2017-09-09T10:19:05Z

[extra]
duration = "32:36"
mp3 = { filename = "LaBoRen-P5-kial-gravas-viziti-la-ejon-20170909-100051.mp3", size = 33640197 }
mp3_lofi = { filename = "LaBoRen-P5-kial-gravas-viziti-la-ejon-malpeza-20170909-100516.mp3", size = 7824405 }
ogg = { filename = "LaBoRen-P5-kial-gravas-viziti-la-ejon-20170909-100558.ogg", size = 24871516 }

[[extra.comments]]
pk = 5
name = "Stela Besenyei-Merger"
color = "hsl(353, 100%, 41%)"
submit_date = "2017-09-09T10:19:58.896622"
comment = """Kion mi forgesis? Pri kiuj aferoj ankoraŭ vi povus demandi aŭ atenti dum vizitado de la ejo?"""
+++

Vizitado de la ejo signifas multe pli ol vi pensas. Kompreneble, vi povas havi bonan ideon pri la ejo de via futura renkontiĝo tra la retejo, vidante kelkajn fotojn kaj priskribon pri la ĉambroj, komunaj ejoj. 
Tamen estas tute alia afero fakte esti surloke kaj persone povi sperti la etoson. 

Ne nur pro tio gravas viziti. Havi personan kontakton kun la estroj de la junulargastejo aŭ kampadejo pli rapidigas la organizadon, ricevi respondojn surloke al mil demandoj ŝparas al via teamo multe da tempo kaj energio, aldona komunikado.

Scii kiom fakte daŭras atingi la ejon, aŭ kiom longa estas vere la distanco inter ejoj donos al vi multe pli bonan ideon kiel plani vian programon. Aldone, vizitado de la urbo, ĉirkaŭaĵo donas al vi la eblecon havi pliajn ideojn por programeroj, ekskursoj. Uzu la eblecon renkonti la lokajn Esperantistojn, viziti la turistoficejon kaj eventuale ankaŭ la urbestron aŭ lokajn oficistojn.

