+++
title = "Havu informplenan retpaĝon!"
path = "13-retpagxo"
aliases = ["13"]
weight = 13
date = 2017-12-13T09:55:43Z

[extra]
duration = "22:27"
mp3 = { filename = "LaBoRen-P13-retpagxo-20171213-094453.mp3", size = 22962439 }
mp3_lofi = { filename = "LaBoRen-P13-retpagxo-malpeza-20171213-094453.mp3", size = 10778957 }
ogg = { filename = "LaBoRen-P13-retpagxo-20171213-094453.ogg", size = 16988901 }
+++

Multe da renkontiĝoj havas retpaĝon, sed ĉu bonan? Kiajn informojn vi aperigu, kio gravas?

Kio estas la plej granda miskompreno rilate al retpaĝoj? Kiuj estas tiuj du tute malsamaj taskoj kiuj ligiĝas al ĝi?

Konsiloj pri diversaj partoj de la retejo, listo de kiajn informojn viaj partoprenontoj bezonas.

La nova babilejo en telegramo troviĝas ĉi tie: https://t.me/joinchat/EbOV0RI2aECQRfxcaOokmA

