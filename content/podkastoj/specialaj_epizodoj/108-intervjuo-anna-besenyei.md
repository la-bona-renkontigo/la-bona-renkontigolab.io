+++
title = "La panjo de Stela – intervjuo kun Anna Besenyei"
path = "108-intervjuo-anna-besenyei"
aliases = ["108"]
weight = 108
date = 2018-07-19T08:19:01Z

[extra]
duration = "39:02"
mp3 = { filename = "LaBoRen-P108-intervjuo-anna-besenyei-20180717-185655.mp3", size = 75534172 }
mp3_lofi = { filename = "LaBoRen-P108-intervjuo-anna-besenyei-malpeza-20180717-190051.mp3", size = 18737527 }
ogg = { filename = "LaBoRen-P108-intervjuo-anna-besenyei-20180717-191058.ogg", size = 34575707 }

[[extra.comments]]
pk = 5657
name = "Nelli"
color = "hsl(309, 100%, 41%)"
submit_date = "2018-10-19T11:00:31.295979"
comment = """Dankon pro tiaj preciozaj atestoj !"""
+++

Ekkonu la panjon de "la voĉo de LaBoRen"!  Pri mia denaskuleco demandas mi la plej proksiman fonton kiun eblas: venu la lasta intervjuo antaŭ la somera paŭzo, kun mia panjo, Anna Besenyei.

En tiu ĉi epizodo vipovos aŭdi:

- pri kiel mia patrino reagis al tio, ke malgraŭ tio, ke mi estas denaskulo, mi neniam respondis Esperante al ŝi,
- reagojn al ĝeneralaj demandoj kiujn mi ricevas pri denaskuleco,
- kiajn riproĉojn mia panjo ricevis pro instrui Esperanton al mi,
- kiujn lingvojn miaj geavoj kaj mia patrino parolas/parolis, kiel influis kiujn lingvojn (ne) lerni la dua mondmilito,
- kiom da jaroj pasis inter kiam mia patrino la unuan fojon aŭdis pri Esperanto kaj ĝis ŝi finfine decidis lerni ĝin,
- malgraŭ la entuziasmo kial mia panjo ne uzis la lingvon dum 7-8 jaroj post ĝis lernado,
- pri partoprenado kaj organizado de renkontiĝoj,
- kial la Renkontiĝoj de Esperanistaj Familioj okazis tiom altnombre en Hungario, do fakte „ĉe ni”,
- kial ni fine ĉesis paroli Esperanton en la familio,

kaj ankoraŭ multe-multe pli.

La ligilo al la enketilo pri la unua jaro de LaBoRen estas: https://tinyurl.com/y86xy6s2 Antaŭdankojn por viaj respondoj!

Ripozigan someran paŭzon al vi!

