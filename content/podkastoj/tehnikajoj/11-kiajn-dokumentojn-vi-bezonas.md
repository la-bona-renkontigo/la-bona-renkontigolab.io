+++
title = "Kiajn dokumentojn vi bezonas?"
path = "11-kiajn-dokumentojn-vi-bezonas"
aliases = ["11"]
weight = 11
date = 2017-12-01T10:46:53Z

[extra]
duration = "30:24"
mp3 = { filename = "LaBoRen-P11-kiajn-dokumentojn-vi-bezonas-20171201-104653.mp3", size = 31808909 }
mp3_lofi = { filename = "LaBoRen-P11-kiajn-dokumentojn-vi-bezonas-malpeza-20171201-104653.mp3", size = 14594088 }
ogg = { filename = "LaBoRen-P11-kiajn-dokumentojn-vi-bezonas-20171201-104653.ogg", size = 23378536 }
+++

Mi ŝparos por vi multe da tempo per tiu ĉi epizodo! Vi aŭdos kiel elekti dokumentujon kiu pleje kongruas kun via stilo de organizado. Kiajn bazajn dokumentojn vi bezonas por la organizado?

La ekzemplo kiun mi uzas estas mia metodo por trovi la ĝustan ejon de renkontiĝo. Mi montros kial gravas klara kolektado de informoj por la teamo. Vi komprenos kial kaj kiel pli faciligos la organizadon bona sistemigo de la dokumentoj. Teda temo? Eble. Kerna parto de organizado? Jes!

