dayjs.locale("eo")
/**

 * Formats a date string from RFC2822 format to ISO 8601 or a custom readable format.
 *
 * @param {string} dateString - The date string in RFC2822 format (e.g., "Tue, 01 Jun 2024 10:00:00 GMT").
 * @param {string} [format="readable"] - The desired output format.
 *   - "iso": Returns the date in ISO 8601 format (YYYY-MM-DD).
 *   - "readable": Returns the date in a human-readable format (e.g., "June 1, 2024").
 *   - Any other string: Returns the date in ISO 8601 format.
 *
 * @returns {string} The formatted date string.  Returns "Invalid Date" if the input
 *   dateString cannot be parsed.
 *
 * @example
 * formatDate("Tue, 01 Jun 2024 10:00:00 GMT", "iso"); // Returns "2024-06-01"
 * formatDate("Tue, 01 Jun 2024 10:00:00 GMT", "readable"); // Returns "June 1, 2024"
 * formatDate("Invalid Date String", "iso"); // Returns "Invalid Date"
 */
function formatDate(dateString) {
  // Parse the RFC2822 date
  const date = new Date(dateString)

  // Check if the date is valid
  if (Number.isNaN(date.getTime())) {
    console.error(`Invalid date: ${dateString}`)
    return dateString
  }
  return dayjs(date).locale("eo").format("D[-a de] MMMM YYYY")
}

// Find and replace all RFC2822 dates in the document
function replaceDatesInDocument() {
  // Target elements with the 'rfc2822-date' class
  const dateElements = document.querySelectorAll(".rfc2822-date")

  dateElements.forEach((element) => {
    const originalDate = element.getAttribute("datetime").trim()
    element.textContent = formatDate(originalDate)
  })
}

// Run when the DOM is fully loaded
document.addEventListener("DOMContentLoaded", replaceDatesInDocument)
