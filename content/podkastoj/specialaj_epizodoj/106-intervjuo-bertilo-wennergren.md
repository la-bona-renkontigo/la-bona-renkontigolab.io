+++
title = "Rokstelulo de du mondoj - intervjuo kun Bertilo Wennergren"
path = "106-intervjuo-bertilo-wennergren"
aliases = ["106"]
weight = 106
date = 2018-05-28T19:41:21Z

[extra]
duration = "57:59"
mp3 = { filename = "LaBoRen-P106-intervjuo-bertilo-wennergren-20180528-183712.mp3", size = 115066422 }
mp3_lofi = { filename = "LaBoRen-P106-intervjuo-bertilo-wennergren-malpeza-20180528-183322.mp3", size = 27834826 }
ogg = { filename = "LaBoRen-P106-intervjuo-bertilo-wennergren-20180528-183110.ogg", size = 49120872 }

[[extra.comments]]
pk = 111
name = "László Vizi"
color = "hsl(4, 100%, 41%)"
submit_date = "2018-06-03T02:56:11.586168"
comment = """La kvalito de voĉo de Bertilo pro la pertelefona sonregistrado estas malalta. Foje mi proponis, kaj mi ripetas mian proponon: ĉe skajpraporto AMBAUX partneroj faru rektan sonregistrajxon pri la konversacio, poste oni kunmuntu la du registrajxojn. Tiel la kvalito de ambaux voĉoj estos alta.<br>
<br>
Amike: Laĉjo"""
+++

Stela intervjuas Bertilo Wennergren, kiu iĝis konata unue kiel rokmuzikisto en la Esperanto-rokbandoj Amplifiki kaj Persone. Nuntempe lia nomo pli ofte aperas rilate al Esperanto-gramatikaĵoj. Nome PMEG, aŭ Pomego (Plena Manlibro de Esperanta Gramatiko), kies verkanto li estas.

El tiu ĉi babilado interalie vi ekscios pli:
- pri denaskuleco;
- kio konvinkis lin lerni Esperanton?
- kial li lernis ankaŭ la svedan signolingvon?
- kompreneble pri Persone kaj Amplifiki;
- pri PMEG, kvereloj en la Esperanto-komunumo rilate al gramatiko kaj eĉ pri neduumaj Esperantistoj.
- kaj pri la sekreta revo, kion li ankoraŭ volonte farus en la Esperanta movado. Li ankoraŭ nenie parolis pri tio, do eble la respondo tre surprizos vin.

Por detaloj pri la menciitaj organizoj, temoj, homoj, kaj aliaj movadaj rimarkoj vidu la blogon de Stela:
https://stelachiamnurkritikas.wordpress.com/2018/05/29/stelulo-de-du-mondoj-intervjuo-kun-bertilo-wennergren/

