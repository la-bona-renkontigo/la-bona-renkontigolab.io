+++
title = "Novaĵoj"
+++

<h4>Kio okazas nuntempe en la mondo de Stela?</h4>

<p>Vi vidos ĉi tie la koncizajn raportojn pri kiu projektoj mi okupiĝas nuntempe.
</p>

<h5>2025 februaro</h5>

<p>Ĵus mi aĉetis flugbiletojn por partopreni la Britan Kongreson en Manĉestro inter la 4-a kaj 7-a de aprilo!
</p>

<h5>2025 januaro</h5>

<p>Mi partoprenis en la surbendigo de nova epizodo de Usone Persone. Vi povas aŭskulti ĝin ĉi tie: <a href="https://usonepersone.substack.com/p/35-lipe-kun-la-esperantisto-de-la">Usone Persone 3.5 - LIPE (kun la Esperantisto de la Jaro, Stela B-M)</a>
</p>

<h5>2024 novembro</h5>

<p>Regule mi komencis verki pri ĉiutagaĵoj en mia eta blogo ĉe Substack: <a href="https://stelabm.substack.com/">Stela havas opinion pri la mondo</a>
</p>

<h4>Dankon, ke vi interesiĝas pri miaj projektoj!</h4>