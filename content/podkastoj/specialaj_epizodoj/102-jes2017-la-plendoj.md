+++
title = "JES 2017/2018 - la plendoj!"
path = "102-jes2017-la-plendoj"
aliases = ["102"]
weight = 102
date = 2018-01-10T10:04:32Z

[extra]
duration = "32:23"
mp3 = { filename = "LaBoRen-P102-jes-20172018-la-plendoj-20180109-213337.mp3", size = 41462587 }
mp3_lofi = { filename = "LaBoRen-P102-jes-20172018-la-plendoj-malpeza-20180109-213337.mp3", size = 15542229 }
ogg = { filename = "LaBoRen-P102-jes-20172018-la-plendoj-20180109-213337.ogg", size = 24565710 }

[[extra.comments]]
pk = 86
name = "Devid"
color = "hsl(242, 100%, 41%)"
submit_date = "2018-01-11T18:49:01.433660"
comment = """Dankon pro via laboro, via ateliero dum la JES kaj viaj podkastoj estas tre valoraj!"""

[[extra.comments]]
pk = 87
name = "Charlotte"
color = "hsl(271, 100%, 41%)"
submit_date = "2018-01-12T16:20:05.302574"
comment = """Dankon pro via podkasto! 
Mi bedaŭrinde ne povis ĉeesti viajn programerojn, sed mi esperas ke mi iam baldaŭ dum estonta renkontiĝo povos!"""

[[extra.comments]]
pk = 88
name = "Anna"
color = "hsl(216, 100%, 41%)"
submit_date = "2018-01-20T13:20:27.645109"
comment = """Malfrua kaj mankohami konsentas kun avida aligxilo. Nur post iom da tempo evidentigxis, ke oni devas antaupagi kaj kiom.<br>
Malgrau ĉiuj fusxoj mi gxuegis la renkontigxon.<br>
Via ateliero estis bonega, mi konsentas kun David"""
+++

JES 2017-2018 okazis en Szczecin, Pollando, kaj ĝi estis bona! Sed tamen, la organizantoj faris kelkajn sufiĉe grandajn erarojn. En tiu ĉi epizodo sincere kaj direkte dispecigos tiujn fuŝojn. Proponos kelkajn aferojn kion fari aŭ ne fari la sekvan fojon.

Kiu plendo mankas? Ĉu estis io kio al vi ne plaĉis, sed mi ne menciis?

