+++
title = "JES 2017/2018 - la laŭdoj!"
path = "101-jes2017-laudoj"
aliases = ["101"]
weight = 101
date = 2018-01-10T10:04:15Z

[extra]
duration = "20:40"
mp3 = { filename = "LaBoRen-P101-jes2017-laudoj-20180109-212622.mp3", size = 26881231 }
mp3_lofi = { filename = "LaBoRen-P101-jes2017-laudoj-malpeza-20180109-212622.mp3", size = 9916916 }
ogg = { filename = "LaBoRen-P101-jes2017-laudoj-20180109-212622.ogg", size = 15646787 }

[[extra.comments]]
pk = 85
name = "Charlotte"
color = "hsl(271, 100%, 41%)"
submit_date = "2018-01-10T16:53:34.413867"
comment = """Dankon pro la bela podkastepizodo! Mi ankaŭ ege ĝuis la JESon pro multaj el la kialoj, kiujn vi menciis."""
+++

JES 2017-2018 okazis en Szczecin, Pollando, kaj ĝi estis tre bonetosa! Venu unue la laŭdoj de la renkontiĝo, kion faris bone la organizantoj? Kion indas kopii de ili?

Ĉu estas laŭdo, bonaĵo kiu mankas? Kio plaĉis al vi dum la JES?

