+++
title = "Post la reta IJK"
path = "110-post-ijk-reta"
aliases = ["110"]
weight = 110
date = 2020-08-01T13:00:00Z

[extra]
duration = "52:39"
mp3 = { filename = "LaBoRen-P110-postIJK.mp3", size = 126381632 }
mp3_lofi = { filename = "LaBoRen-P110-postIJK_malpeza.mp3", size = 25278080 }
ogg = { filename = "LaBoRen-P110-postIJK.ogg", size = 41434595 }
+++

En la enkonduka epizodo pri la reta IJK kie mi parolis pri miaj duboj pri organizado de reta renkontiĝo tiel ambicia kiel IJK. 

Nun mi prezentas al vi rezulton. 
Kiel okazis la IJK? Kiaj estis la spertoj? Kiuj programeroj sukcesis la plej bone? Kian kritikon mi povas trovi.

Speciala parto de tiu ĉi epizodo estas, ke pro ĝia longeco mi aldonis muzikan paŭzon. Vi povos aŭskulti la gajninton de la Mondvizio!


