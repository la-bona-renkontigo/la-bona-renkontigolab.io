+++
title = "Aŭskultu"
+++

<h4>Kie eblas aŭskulti la epizodojn?</h4>

<p>Ĉi tie! Vi estas en la bona loko. Maldekstre klakante sur ajna epizodo aperas sia paĝo kaj vi tuj vidas la priskribon kaj intertempe povas aŭskulti ĝin.
</p>

<h4>Ĉu vi volas aŭskulti poste?</h4>

<p>Elŝutu la epizodojn! Eblas en altkvalita MP3 kaj malpeza MP3 versioj. La butonoj por elŝuti estas tuj sub la priskribo de la epizodo.
</p>
<p>Se vi havas podkasto-aŭskultilon sur via saĝa telefono, do uzu ĝin por trovi nin! 
Simple enskribu "La Bona Renkontiĝo" (uzu la plenan nomon) kaj vi tuj trovos nin.

Ekzemple en Stitcher, Podcast Addict kaj iTunes.
</p>
<p>Alternative vi povas enskribi la direktan retadreson: <a href="https://www.laboren.org/rss.xml">https://www.laboren.org/rss.xml</a></p>

<p>Dankon, ke vi aŭskultas #LaBoRen!</p>
