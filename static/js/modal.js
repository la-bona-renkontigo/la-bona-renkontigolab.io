document.addEventListener("DOMContentLoaded", function () {
  var MODAL_ID = "modal-aboni";

  function sessionClose(modal) {
    modal.classList.remove("is-active");
    if (!sessionStorage.getItem(MODAL_ID)) {
      sessionStorage.setItem(MODAL_ID, Date());
    }
  };

  function localClose(modal) {
    modal.classList.remove("is-active");
    if (!localStorage.getItem(MODAL_ID)) {
      localStorage.setItem(MODAL_ID, Date());
    }
  };

  var $modal = document.getElementById(MODAL_ID);

  if ($modal) {
    document.addEventListener("keydown", function (e) {
      if (e.keyCode === 27) {
        sessionClose($modal);
      }
    });
  }

  document.addEventListener("mouseout", function (e) {
    e = e ? e : event;
    if (!sessionStorage.getItem(MODAL_ID) && !localStorage.getItem(MODAL_ID)) {
      if ($modal && e.toElement == null && e.relatedTarget == null && e.y < 1) {
        $modal.classList.add("is-active");
      }
    }
  });

  var $hideOnClick = [].concat(
    Array.prototype.slice.call(
      document.querySelectorAll(".modal-background"),
      0
    ),
    Array.prototype.slice.call(document.querySelectorAll(".modal__close"), 0)
  );

  if ($modal && $hideOnClick.length > 0) {
    $hideOnClick.forEach(function (el) {
      el.addEventListener("click", function () {
        sessionClose($modal);
      });
    });
  }

  var $close = Array.prototype.slice.call(
    document.querySelectorAll(".modal__never_again"),
    0
  );

  if ($modal && $close.length > 0) {
    $close.forEach(function (el) {
      el.addEventListener("click", function () {
        localClose($modal);
      });
    });
  }

  var $form = Array.prototype.slice.call(
    document.querySelectorAll(".form__subscribe"),
    0
  );

  if ($modal && $form.length > 0) {
    $form.forEach(function (el) {
      el.addEventListener("submit", function () {
        localClose($modal);
      });
    });
  }
});
