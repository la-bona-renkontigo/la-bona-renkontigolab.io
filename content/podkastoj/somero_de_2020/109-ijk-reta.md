+++
title = "IJK reta"
path = "109-ijk-reta"
aliases = ["109"]
weight = 109
date = 2020-07-06T18:08:00Z

[extra]
duration = "28:25"
mp3 = { filename = "LaBoRen-P109-ijk-reta-20200706-180800.mp3", size = 68229504 }
mp3_lofi = { filename = "LaBoRen-P109-ijk-reta-malpeza-20200706-180800.mp3", size = 13646016 }
ogg = { filename = "LaBoRen-P109-ijk-reta-20200706-180800.ogg", size = 22347331 }
+++

Mi parolas pri la sperto organizi la unuan retan IJK-on! Ŝanĝi de persona renkontiĝo al reta, kiel fari tion? Kio estas tio? Kiaj defioj aperas? Kiaj eblecoj aperas kiuj ne ekzistis antaŭe? Kio same malfacilas? Pri kio oni plendas nun?

Mi esperas, ke tiu ĉi epizodo inspiros vin por konsideri aliĝi al IJK 2020.
