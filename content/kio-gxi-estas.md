+++
title = "Kio estas La Bona Renkontiĝo?"
+++

<p>
    La Bona Renkontiĝo estas libere elŝutebla podkasto-projekto de Stela Besenyei-Merger. La ideo jam naskiĝis antaŭ longa tempo, kaj la celo estas transdoni spertojn al la nova generacio de organizantoj. <strong>#LaBoRen</strong> celas instrui kaj konsili pri la tuta organiza proceso de la komenca ideo ĝis la detaloj per aŭskultado de diversaj temoj. En la epizodoj vi povas aŭdi spertojn de Stela pri aranĝoj, intervjuojn kun aktivuloj en la Esperanto-movado, la invititoj buntigas la epizodojn donante pliajn perspektivojn pri kio okazas en Esperantujo.
</p>

<p>
 La podkastoj starigas multajn konsiderindajn demandojn. Stela rakontas pri la petoj kaj defioj de organizado ĉiam fokusiĝante pri unu temo. La dosieroj estas elŝuteblaj en altkvalita kaj ankaŭ en simpligita versioj por ebligi la aŭskultadon por ĉiu.
</p>