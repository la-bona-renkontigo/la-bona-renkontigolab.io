+++
title = "Kion faras ĉeforganizanto?"
path = "2-kion-faras-ceforganizanto"
aliases = ["2"]
weight = 2
date = 2017-08-01T11:02:34Z

[extra]
duration = "32:02"
mp3 = { filename = "LaBoRen-P2-kion-faras-ceforganizanto-20170801-113156.mp3", size = 30728776 }
mp3_lofi = { filename = "LaBoRen-P2-kion-faras-ceforganizanto-malpeza-20170801-102004.mp3", size = 7682090 }
ogg = { filename = "LaBoRen-P2-kion-faras-ceforganizanto-20170801-202027.ogg", size = 23918363 }

[[extra.comments]]
pk = 3
name = "Stela Besenyei-Merger"
color = "hsl(353, 100%, 41%)"
submit_date = "2017-08-22T09:49:14.293827"
comment = """Ĉu vi jam estis ĉeforganizanto? Se jes, kia estis via sperto? Se ne, kial ne?"""
+++

Unu el la plej interesaj misteroj de la Esperanto-movado laŭ mi estas pensi ke oni povas havi renkontiĝon sen ĉeforganizanto. En tiu ĉi epizodo mi provas konvinki vin ke ne komencu serioze organizi renkontiĝon sen decidi, kiu estos la ĉeforganizanto de via evento.

Mi klarigas kial oni bezonas iun, kiu kunordigas la eventon. Kaj kio okazas, se vi ne havas tiun personon, kiuj problemoj aperos iam sendube? Kiel elekti la personon por la laboro?

En la dua parto de la podkasto (komence de la 17a minuto) mi klarigos kiajn taskojn havas ĉeforganizanto. Kion oni devas fari, kaj kion ne? Kial oni kutime ne volas esti ĉeforganizantoj?

Fine mi konsilas al la teammembroj kiel vi povas plej bone helpi la estron de via teamo por ke vi havu bonan renkontiĝon.

