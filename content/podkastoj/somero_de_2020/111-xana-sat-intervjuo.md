+++
title = "Neŭtralismo ne estas tiom neŭtrala – intervjuo kun Xana Paz"
path = "111-xana-sat-intervjuo"
aliases = ["111"]
weight = 111
date = 2020-08-20T13:00:00Z

[extra]
duration = "48:15"
mp3 = { filename = "LaBoRen-P111-Xana-SAT-intervjuo.mp3", size = 115819200 }
mp3_lofi = { filename = "LaBoRen-P111-Xana-SAT-intervjuo_malpeza", size = 23163840 }
ogg = { filename = "LaBoRen-P111-Xana-SAT-intervjuo.ogg", size = 43065833 }
+++

Xana estas portugala aktivulo de 20 jaroj. Ni babilados pri tio kion ŝi faras en SAT. Kiel tio rilatas al esti neŭtrala. Aŭ al formaleco. Ni parolos pri la loka movado en ŝia urbo, Oporto, Portugalio. Ŝi rakontas kiel interesaj diskutoj dum renkontiĝoj montras la valoron de Esperanto, ĉar vi povas ekhavi informojn kiujn vi ne povas legi en la novaĵoj.

Ankaŭ ni parolos pri tio kial ŝi ne partoprenis Universalan Kongreson en Lisbono, eĉ se ĝi okazis hejme. Kaj kompreneble pri multe pli, do aŭskultu la epizodon.
