+++
title = "Kiuj estas en la teamo de LaBoRen?"
+++

<div class="columns">
  <div class="column is-one-third">
    <img src="/img/Stela_Besenyei-Merger_2025.jpeg" alt="Bildo de Stela">
  </div>
  <div class="column">
    <p>
      Saluton! Mi estas <a href="https://stelachiamnurkritikas.wordpress.com/about/">Stela</a>
      <i class="fa fa-smile-o" style="vertical-align: baseline"></i>
      <br>
      Mi kreskis kun Esperanto. Mi partoprenas kaj organizas Esperanto-renkontiĝojn ekde mia juneco. En la podkastoj mi transdonas
      al vi paŝo post paŝo miajn spertojn pri organizado de junularaj renkontiĝoj. Por pli bone ekkoni mian laboron
      kaj kion mi celas per miaj #LaBoRen podkastoj aŭskultu <a href="/0-bonvenon/">la enkondukan epizodon ĉi-tie</a>.
    </p>
    <p>
      Helpas al mi <a href="https://eo.wikipedia.org/w/index.php?title=Baptiste_Darthenay&action=edit&redlink=1">Baptiste Darthenay</a>, kiu zorgas pri la retejo, kaj <a href="https://eo.wikipedia.org/wiki/Melono">Melono (Mélaine Desnos)</a>, kiu helpas
      per teĥnikaj konsiloj por bona sonkvalito kaj tranĉado de la epizodoj.
    </p>
  </div>
</div>
