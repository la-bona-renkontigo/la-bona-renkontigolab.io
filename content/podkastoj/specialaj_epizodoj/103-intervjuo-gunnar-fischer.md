+++
title = "Denaskuloj inter si – intervjuo kun Gunnar R. Fischer"
path = "103-intervjuo-gunnar-fischer"
aliases = ["103"]
weight = 103
date = 2018-02-19T10:32:05Z

[extra]
duration = "52:49"
mp3 = { filename = "LaBoRen-P103-surprizo-20180218-212034.mp3", size = 76475996 }
mp3_lofi = { filename = "LaBoRen-P103-surprizo-malpeza-20180219-091841.mp3", size = 25352149 }
ogg = { filename = "LaBoRen-P103-surprizo-20180219-091938.ogg", size = 48797766 }
+++

Gunnar R. Fischer, pli bone estas konata kiel Dĵ Kunar en la Esperanto-movado. Estas honoro, ke li iĝis la unua persono kiun por tiu ĉi podkasto Stela intervjuas. 

Gunnar estas denaskulo el Germanio kaj eksa-prezidanto de la Germana Esperanto-Junularo. Li estis organizanto de la legenda Internacia Seminario, kiu estis la novjara unusemajna renkontiĝo de GEJ. La amikeco de Gunnar kaj Stela estas jam preskaŭ 20-jara.

El tiu ĉi babilado vi ekscios pli pri kion signifas esti denaskulo, ni pridiskutas Esperanto-muzikon, diskotekon, la diferencojn inter junularaj Esperanto-renkontiĝoj antaŭ 20 jaroj kaj nun.

Vi ricevos ankaŭ multe multe da elstaraj konsiloj por via organizado!

Por detaloj pri la menciitaj temoj, homoj, muzikpecoj kaj aliaj movadaj rimarkoj vidu la blogon de Stela:
https://stelachiamnurkritikas.wordpress.com/2018/02/19/denaskuloj-inter-si-intervjuo-kun-gunnar-r-fischer/

