+++
title = "Kontaktu LaBoRen!"
+++
<p>
    Se vi havas demandon pri organizado de renkontiĝoj surloke aŭ rete, se vi havas ideon pri kio bezonatas aldona epizodo, klarigo pri ajna teamo rilate al organizado, simple demandu Stela! Ni volonte helpos al vi realigi viajn renkontiĝo-revojn per praktikaj konsiloj de subtenema teamo.

    Ni esperas, ke vi jam abonas la novaĵleteron por ricevi informojn pri nuntempaj projektoj kaj aktivaĵoj.

<p>
    <i class="fab fa-telegram" style="font-size: 2em; color: CornflowerBlue"></i> <a href="https://t.me/LaBonaRen" target="_blank">#LaBoRen informkanalo </a>- tuja mesaĝo pri la plej nova epizodo en telegramo
</p>
<p>
    <i class="fab fa-telegram" style="font-size: 2em; color: CornflowerBlue"></i> <a href="https://t.me/joinchat/EbOV0RI2aECQRfxcaOokmA" target="_blank">#LaBoRen-babilejo en telegramo </a>- demandu, babiladu, prisdiskutu renkontiĝojn kun aliaj organizantoj!

</p>
<p>
Se vi prefere skribus al ni retleteron: <strong>stela[@]laboren.org</strong>
</p>
<p>
    Se vi deziras <strong>personan konsilon</strong> rilate al via renkontiĝo, skribu al Stela! Ni rigardu kune kiel ni povas solvi vian defion rilate al organizado. La konsiloj de Stela estas praktikaj kaj subtenemaj. 
</p>